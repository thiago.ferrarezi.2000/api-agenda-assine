package com.tarefas.DTO;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.tarefas.Model.Tarefas;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor

@JsonFormat(shape = JsonFormat.Shape.ARRAY)
@JsonPropertyOrder({ "tarefa","descricao","area","data_entrega"})
public class TarefasDTO {

    private Long id;
    private String tarefa;
    private String descricao;
    private String area;
    private Date data_criacao;
    private Date data_entrega;


    
}
