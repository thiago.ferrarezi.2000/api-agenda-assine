package com.tarefas.Service;

import java.util.List;
import java.util.Optional;

import com.tarefas.Builder.TarefasBuilder;
import com.tarefas.DTO.TarefasDTO;
import com.tarefas.Model.Tarefas;
import com.tarefas.Repository.TarefasRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TarefaService {

    @Autowired private TarefasRepository repository;
    @Autowired private TarefasBuilder builder;


    public List<Tarefas> findAll(){
        return repository.findAll();
    } 
        
    public void delete(Tarefas tarefas){
        repository.delete(tarefas);
    }

    public TarefasDTO update(Tarefas tarefas,TarefasDTO dto){
        builder.build(tarefas, dto); 
        repository.save(tarefas);
        return dto;

    }
    public Optional<Tarefas> listById(Long id){
        return repository.findById(id);
    }

    public TarefasDTO saveTarefas(TarefasDTO dto){
        Tarefas tarefas = new Tarefas();
        builder.build(tarefas,dto);
        repository.save(tarefas);
        return dto;
    }
}
