package com.tarefas.Controllers;


import com.tarefas.DTO.TarefasDTO;
import com.tarefas.Model.Tarefas;

import com.tarefas.Service.TarefaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/tarefa")
public class TarefasController {

    @Autowired
    private TarefaService service;

    @GetMapping("/listartarefas")
    public ResponseEntity<?> list() {
        return ResponseEntity.ok().body(service.findAll());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletar(@PathVariable("id") Tarefas tarefas) {
        Assert.notNull(tarefas, "Tarefa não encontrada!");
        service.delete(tarefas);
        return ResponseEntity.ok("Tarefa deletada com sucesso!");
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> atualizar(@PathVariable("id") Tarefas tarefas, @RequestBody TarefasDTO dto) {
        Assert.notNull(tarefas, "Tarefa não encontrada");
        service.update(tarefas, dto);
        return ResponseEntity.ok("Tarefa atualizada com sucesso!");
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> listarPorId(@PathVariable("id") Tarefas tarefas) {
        Assert.notNull(tarefas, "Tarefa não encontrada!");
        return ResponseEntity.ok().body(tarefas);
    }

    @PostMapping("/adicionar")
    public ResponseEntity<?> salvarTarefas(@RequestBody TarefasDTO dto) {
        return ResponseEntity.ok().body(service.saveTarefas(dto));
    }
}

    /////////////////////////////////////////////////////////////////////////////

    ///@PostMapping
    //public Tarefas addEntity(@RequestBody Tarefas tarefas){
        //return service.salvarEntidades(tarefas);
   // }
/*
    @GetMapping("/all/{id}")
    public Optional<Tarefas> findTarefasPorId(@PathVariable Long id){
        return service.encontrarporId(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Tarefas> findTarefas(Tarefas tarefas){
        return repository.findAll();
    }*/

    
//}
