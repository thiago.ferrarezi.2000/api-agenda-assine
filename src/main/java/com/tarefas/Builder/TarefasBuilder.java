package com.tarefas.Builder;

import java.util.Date;
import java.util.Optional;

import javax.transaction.Transactional;

import com.tarefas.DTO.TarefasDTO;
import com.tarefas.Model.Tarefas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TarefasBuilder {

   @Transactional
    public void build(Tarefas tarefas, TarefasDTO dto){
        tarefas.setTarefa(dto.getTarefa());
        tarefas.setDescricao(dto.getDescricao());
        tarefas.setArea(dto.getArea());
        tarefas.setDataCriacao(new Date());
        tarefas.setDataEntrega(dto.getData_entrega());
    }
    
}
