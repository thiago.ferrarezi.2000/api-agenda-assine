package com.tarefas.Repository;

import com.tarefas.DTO.TarefasDTO;
import com.tarefas.Model.Tarefas;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TarefasRepository extends JpaRepository<Tarefas,Long> {
}
