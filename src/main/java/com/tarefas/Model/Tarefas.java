package com.tarefas.Model;

import java.util.Date;

import javax.persistence.*;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor

@Table(name = "tbl_tarefa")
@Entity
public class Tarefas {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String tarefa;

    private String solicitante;

    @NotNull
    private String descricao;

    @NotNull
    private String area;

    @NotNull
    @Column(name = "data_criacao")
    private Date dataCriacao;
    
    @NotNull
    @Column(name ="data_entrega")
    private Date dataEntrega;

    
}
