CREATE TABLE IF NOT EXISTS tbl_tarefa  (
  id                       BIGINT(20)     NOT NULL AUTO_INCREMENT,
  data_criacao             DATETIME       NOT NULL,

  tarefa                   VARCHAR(255)   NULL,
  solicitante              VARCHAR(255)   NULL,
  descricao                VARCHAR(255)   NULL,
  area                     VARCHAR(20)    NOT NULL,
  data_entrega             DATETIME       NOT NULL,

  PRIMARY KEY (id)
)
ENGINE = InnoDB;